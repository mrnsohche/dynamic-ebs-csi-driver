
output "identity-oidc-issuer" {
  value = data.aws_eks_cluster.cluster.identity[0].oidc[0].issuer
}

output "aws_iam_role" {
    value = aws_iam_role.ebs_csi_driver_role.arn
  
}