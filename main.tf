
# #########################################################
# # checking for OIDC if exisit / Create OIDC if not found
# #########################################################

# # data "aws_iam_openid_connect_provider" "oidc_provider" {
# #   # url = "https://oidc.eks.${var.aws_region}.amazonaws.com/id/${var.cluster_name}:${var.oidc_provider_path}"
# # }

# locals {
#   oidc_provider_attached = length(data.aws_iam_openid_connect_provider.oidc_provider.arn) > 0
# }

# resource "aws_iam_openid_connect_provider" "oidc_provider" {
#   count = local.oidc_provider_attached ? 0 : 1

#   client_id_list  = ["${var.cluster_name}:${var.oidc_provider_client_id}"]
#   thumbprint_list = [data.aws_eks_cluster.cluster.certificate_authority[0].data]
#   url             = "https://oidc.eks.${var.aws_region}.amazonaws.com/id/${var.cluster_name}:${var.oidc_provider_path}"
# }

# output "oidc_provider_attached" {
#   value = local.oidc_provider_attached
# }

#########################################################
# creating eks ebs csi driver 
#########################################################

data "aws_partition" "current" {}

data "aws_caller_identity" "current" {}

data "aws_eks_cluster" "cluster" {
  name = var.cluster_name
}

locals {
  partition          = data.aws_partition.current.id
  account_id         = data.aws_caller_identity.current.account_id
  oidc_provider_arn  = replace(data.aws_eks_cluster.cluster.identity[0].oidc[0].issuer, "https://", "")
  oidc_provider_name = "arn:${local.partition}:iam::${local.account_id}:oidc-provider/${local.oidc_provider_arn}"
}

resource "aws_eks_addon" "ebs-csi" {
  cluster_name             = var.cluster_name
  addon_name               = "aws-ebs-csi-driver"
  addon_version            = "v1.17.0-eksbuild.1"
  service_account_role_arn = aws_iam_role.ebs_csi_driver_role.arn
  tags = {
    "eks_addon" = "ebs-csi"
    "terraform" = "true"
  }
}
