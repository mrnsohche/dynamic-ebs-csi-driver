
variable "component_name" {
  type        = string
  description = "(optional) describe your variable"
  default     = "my-eks"

}

variable "cluster_name" {
  description = "The name of the Amazon EKS cluster"
  default     = "education-eks-GiAlWyYH"
}

# variable "aws_region" {
#   description = "The AWS region where the cluster is deployed"
#   default     = "us-east-1"
# }

# variable "oidc_provider_path" {
#   description = "The path of the IAM OIDC provider"
#   default     = "k8s/my-eks-cluster"
# }

# variable "oidc_provider_client_id" {
#   default = "sts.amazonaws.com"
# }


# variable "kubernetes_service_account_name" {
#   description = "kubernetes service account name"
#   default     = "ebs-csi-controller-sa"
# }

# variable "kubernetes_namespace" {
#   description = "kubernetes namespace"
#   default     = "kube-system"
# }

# variable "oidc_provider_arn" {
#   default = "https://oidc.eks.us-east-1.amazonaws.com/id/426811B57048C7E479E1F95E2E98BEDC"
# }

# variable "YOUR_AWS_ACCOUNT_ID" {
#   description = "The ID of your AWS account "
#   default     = "523191819167"
# }


