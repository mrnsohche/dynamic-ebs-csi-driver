###################################################
# creating ebs csi driver policy
###################################################

resource "aws_iam_policy" "ebs_csi_driver_policy" {
  name = format("%s-AmazonEKS_EBS_CSI_Driver_Policy", var.component_name)
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "ec2:AttachVolume",
          "ec2:CreateVolume",
          "ec2:CreateTags",
          "ec2:DeleteTags",
          "ec2:DeleteVolume",
          "ec2:DescribeAvailabilityZones",
          "ec2:DescribeInstances",
          "ec2:DescribeTags",
          "ec2:DescribeVolumes",
          "ec2:DescribeVolumesModifications",
          "ec2:DetachVolume",
          "ec2:ModifyVolume",
          "ec2:DescribeVolumeAttribute",
          "ec2:DescribeVolumeStatus",
          "ec2:DescribeInstanceAttribute"
        ]
        Effect   = "Allow"
        Resource = "*"
      }
    ]
  })
}

###################################################
# creating ebs csi driver role
###################################################

resource "aws_iam_role" "ebs_csi_driver_role" {
  name = format("%s-AmazonEKS_EBS_CSI_DriverRole", var.component_name)

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRoleWithWebIdentity"
        Effect = "Allow"
        Principal = {
          Federated = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:oidc-provider/${local.oidc_provider_arn}"
        }
        Condition = {
          StringEquals = {
            "${local.oidc_provider_arn}:sub" = "system:serviceaccount:kube-system:ebs-csi-controller-sa"
          }
        }
      },
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }
    ]
  })
}

###################################################
# creating ebs csi driver role policy attachment
###################################################


resource "aws_iam_role_policy_attachment" "ebs_csi_driver_role_policy_attachment" {
  policy_arn = aws_iam_policy.ebs_csi_driver_policy.arn
  role       = aws_iam_role.ebs_csi_driver_role.name
}


###################################################
# creating ebs csi driver clusterrolebinding
###################################################

# resource "kubernetes_cluster_role_binding" "ebs_csi_driver_cluster_role_binding" {
#   metadata {
#     name = format("%s-ebs-csi-driver-cluster-role-binding", var.component_name)
#   }

#   role_ref {
#     api_group = "rbac.authorization.k8s.io"
#     kind      = "ClusterRole"
#     name      = "system:persistent-volume-binder"
#   }

#   subject {
#     kind      = "ServiceAccount"
#     name      = var.kubernetes_service_account_name
#     namespace = var.kubernetes_namespace
#   }
# }


# ##################################################################
# # Annotate the ebs-csi-controller-sa Kubernetes service account 
# # with the Amazon Resource Name (ARN) of the IAM role 
# ##################################################################


# resource "kubectl_annotation" "ebs_csi_controller_sa_annotation" {
#   namespace = "kube-system"
#   name      = "ebs-csi-controller-sa"
#   annotations = {
#     "eks.amazonaws.com/role-arn" = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/${format("%s-AmazonEKS_EBS_CSI_DriverRole", var.component_name)}"
#     # "serviceaccount.kubernetes.io/service-account.node-selector" = {"beta.kubernetes.io/os":"linux"}
#   }
# }
