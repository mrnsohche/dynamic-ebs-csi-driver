Description 

This code will Dynamically Create :
- IAM policy for Amazon EBS_CSI_DRIVER
- IAM role, and trust policy 
- Attacthment of IAM policy to IAM role 
- Amazon EBS CSI DRIVER as an AddOn 

Pre-requisites:

- Connect to EKS cluster
- eksctl on local machine
- aws-cli
- Replace values in the variable.tf as need  requires .

Step 1:

Check if OIDC is attached to cluster ec2 make sure to replace <cluster_name> with name of your cluster.

- oidc_id=$(aws eks describe-cluster --name <cluster_name>  --query "cluster.identity.oidc.issuer" --output text | cut -d '/' -f 5)

- aws iam list-open-id-connect-providers | grep $oidc_id | cut -d "/" -f4

Infor: If output is returned, then you already have an IAM OIDC provider for your cluster and you can skip the next step. If no output is returned, then you must create an IAM OIDC provider for your cluster.

Step 2:

RUN CMD TO Create IAM OIDC provide make sure to replace <cluster_name> with the name of your cluster.

- eksctl utils associate-iam-oidc-provider --cluster <cluster_name> --approve

Step 3

RUN...............
- terraform init
- terraform validate
- terraform plan
- terraform apply

Step 4 

Annotate the ebs-csi-controller-sa Kubernetes service account with the Amazon Resource Name (ARN) of the IAM role that will be outputed after terraform apply . Please edit and put in your values as required.

- kubectl annotate serviceaccount ebs-csi-controller-sa \
  -n kube-system \
  eks.amazonaws.com/role-arn=arn:aws:iam::<YOUR_AWS_ACCOUNT_ID>:role/AmazonEKS_EBS_CSI_DriverRole

Step 5

Delete the driver pods, driver pods are automatically redeployed with the IAM permissions from the IAM policy assigned to the role .

- kubectl delete pods \
  -n kube-system \
  -l=app=ebs-csi-controller
