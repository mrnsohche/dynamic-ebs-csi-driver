
terraform {
  required_version = ">=1.1.0"

  # backend "s3" {
  #   bucket = "giterraformbackend"
  #   key    = "ebs-csi/terraform.tfstate"
  #   region = "us-west-1"
  # }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.15.0"
    }
  }

}

provider "kubernetes" {
  config_path = "~/.kube/config"
}


#   required_providers {
#     aws = {
#       source  = "hashicorp/aws"
#       version = ">= 4.15.0"
#     }
#     kubectl = {
#       source  = "hashicorp/kubectl"
#       version = "1.22.2"
#     }
#     kubernetes = {
#       source  = "hashicorp/kubernetes"
#     }
#   }
# }

# provider "aws" {
#   region = var.aws_region
# }

# provider "kubectl" {}

# provider "kubernetes" {}





# provider "kubectl" {
#   config_context_cluster = var.cluster_name
# }


